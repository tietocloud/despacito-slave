#define PLAIN_VERSION "2017.09-rc1-g3e1fde38-dirty"
#define U_BOOT_VERSION "U-Boot " PLAIN_VERSION
#define CC_VERSION_STRING "arm-linux-gnueabi-gcc (Ubuntu/Linaro 6.3.0-12ubuntu2) 6.3.0 20170406"
#define LD_VERSION_STRING "GNU ld (GNU Binutils for Ubuntu) 2.28"
