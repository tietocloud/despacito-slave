#ifndef __DESPACITO_H__
#define __DESPACITO_H__

#include <common.h>
#include <net.h>
#include <command.h>
#include <stdio_dev.h>
#include <stdlib.h>

/*
 * Initialize despacito (beginning of netloop)
 */
void despacito_start(void);

#endif /* __DESPACITO_H__ */
