#include "../net/despacito.h"

#define TIMEOUT		5000UL
#define TIMEOUT_COUNT 20

static uchar ether[6];

static struct in_addr despacito_ip;

//IPs
static char * OUR_IP = "192.168.10.10";
static char * NETMASK = "255.255.255.0";
static char * SERVER_IP = "192.168.10.20";

//Ports
static short OUT_PORT = 4444;
static short IN_PORT = 4444;

//Command
static char * COMMAND = "tftp 0x82000000 uImage; tftp 0x88000000 dtb;tftp 0x90000000 ramdisk; bootm 0x82000000 0x90000000 0x88000000";

//Statuses
static char * SLAVE_OFF = "SLAVE_OFF";
static char * SLAVE_BUSY = "SLAVE_BUSY";
static char * FORCE_BOOT = "FORCE_BOOT";
static char * GETSTATUS = "GETSTATUS";

//Other
static short PORT_LEN = 5;
static int REPLAY_COUNTER = 0;

static void set_settings_to_env(void) {
    char * tmp = (char *)malloc(sizeof(char) * PORT_LEN);

    if(!getenv("ipaddr")) {
	    setenv("ipaddr", OUR_IP);
	}
	
	if(!getenv("serverip")) {
	    setenv("serverip", SERVER_IP);
	}
	
	if(!getenv("netmask")) {
	    setenv("netmask", NETMASK);
	}
	
	if(!getenv("despacito_ip")) {
	    setenv("despacito_ip", SERVER_IP);
	}
	
	if(!getenv("despacito_cmd")) {
        setenv("despacito_cmd", COMMAND);
    }
    
    sprintf(tmp, "%hi", OUT_PORT);
    
    if(!getenv("despacito_out_port")) {
        setenv("despacito_out_port", tmp);
    }
    
    sprintf(tmp, "%hi", IN_PORT);
    
    if(!getenv("despacito_in_port")) {
        setenv("despacito_in_port", tmp);
    }
}

static void read_settings_from_env(void) {
	const char *p;
	
	if (getenv("despacito_ip")) {
		despacito_ip = getenv_ip("despacito_ip");
	}

	p = getenv("despacito_out_port");
	if (p != NULL)
		OUT_PORT = simple_strtoul(p, NULL, 10);

	p = getenv("despacito_in_port");
	if (p != NULL)
		IN_PORT = simple_strtoul(p, NULL, 10);
}

/*static int boot(void) {
    return run_command(getenv("bootcmd"), 0);
}*/

static int boot_image(void) {
    return run_command(getenv("despacito_cmd"), 0);
}

static void wait_arp_handler(uchar *pkt, unsigned dest,struct in_addr sip, unsigned src,unsigned len){

}

static int despacito_send(const char * msg) {
    uchar *pkt;
	net_set_arp_handler(wait_arp_handler);
	pkt = (uchar *)net_tx_packet + net_eth_hdr_size() + IP_UDP_HDR_SIZE;
	memcpy(pkt, msg, strlen(msg));
	net_send_udp_packet(ether, despacito_ip, OUT_PORT, IN_PORT, strlen(msg));
	return 0;
}

static void timeout_handler(void){
	net_set_timeout_handler(TIMEOUT, timeout_handler);
}

static void packet_handler(uchar *pkt, unsigned dest_port, struct in_addr src_ip, unsigned src_port, unsigned len) {
    if (dest_port != IN_PORT || !len) {
        return;	
    }
	
	char * split;
	split = strtok((char *)pkt," ");
	
	if(split == NULL) {
	    return;
	}
	
	char status[50];
	strcpy(status, split);
	
	if(status == NULL) {
	    return;
	}
	
	split = strtok(NULL," ");
	
	if(split == NULL) {
	    return;
	}
	
	int counter = (int)simple_strtoul(split, NULL, 10);
	
	if(counter <= REPLAY_COUNTER) {
	    return;
	}
	
	REPLAY_COUNTER = counter;
	
	if(strcmp((char *)status, GETSTATUS) == 0) {
        despacito_send(SLAVE_OFF);
	}else if(strcmp((char *)status, FORCE_BOOT) == 0) {
        despacito_send(SLAVE_BUSY);
        boot_image();
	}	
}

void despacito_start(void) {
	set_settings_to_env();
	read_settings_from_env();
	net_set_timeout_handler(TIMEOUT, timeout_handler);
	net_set_udp_handler(packet_handler);
	memset(ether, 0, 6);
	
	printf("Using %s device\n", eth_get_name());
	printf("Reading on %pI4\n", &net_ip);
	printf("Port: %d\n", OUT_PORT);
}
